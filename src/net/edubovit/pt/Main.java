package net.edubovit.pt;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {

  public static void main(String[] args) {
    String uri;
    if (args.length > 0) {
      uri = args[0];
    } else if (System.getenv("PT_URI") != null) {
      uri = System.getenv("PT_URI");
    } else {
      throw new IllegalArgumentException("No URI passed");
    }

    int nThreads;
    if (args.length > 1) {
      nThreads = Integer.parseInt(args[1]);
    } else if (System.getenv("PT_THREADS") != null) {
      nThreads = Integer.parseInt(System.getenv("PT_THREADS"));
    } else {
      nThreads = 20;
    }

    System.out.printf("URI: %s%n", uri);
    System.out.printf("Threads: %d%n%n", nThreads);

    PtWorkload[] workloads = new PtWorkload[nThreads];
    var executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(nThreads);
    for (int i = 0; i < nThreads; i++) {
      var workload = new PtWorkload(uri);
      workloads[i] = workload;
      executorService.execute(workload);
    }

    while (true) {
      System.out.println("========================\n");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        System.out.printf("Interrupted - %s%n", e.toString());
      }
      long total = 0;
      for (int i = 0; i < nThreads; i++) {
        System.out.printf("Thread %d:%n", i + 1);
        total += workloads[i].printStats();
      }
      System.out.printf("Total: +%d%n%n", total);
    }
  }

}
