package net.edubovit.pt;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class PtWorkload implements Runnable {

  private final String uri;
  private final Map<Integer, Long> stats = new HashMap<>();
  private long counter = 0;
  private long counterLastValue = 0;

  public PtWorkload(String uri) {
    this.uri = uri;
  }

  @Override
  public void run() {
    var client = HttpClient.newHttpClient();
    var request = HttpRequest.newBuilder(URI.create(uri))
        .timeout(Duration.ofSeconds(120))
        .GET()
        .build();
    while (true) {
      HttpResponse<Void> response = null;
      try {
        response = client.send(request, HttpResponse.BodyHandlers.discarding());
      } catch (IOException | InterruptedException e) {
        System.out.printf("Response error - %s%n", e.toString());
      }
      counter++;
      int statusCode = response == null ? 0 : response.statusCode();
      stats.compute(statusCode, (k, v) -> v == null ? 1 : v + 1);
    }
  }

  public long printStats() {
    long counter = this.counter;
    long diff = counter - counterLastValue;
    System.out.printf("+%d -> %d %s%n%n", diff, counter, stats.toString());
    counterLastValue = counter;
    return diff;
  }

}
